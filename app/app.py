from flask import Flask, render_template

app = Flask(__name__)

@app.route('/')
def home():
    return render_template('home.html')

@app.route('/recipes')
def recipes():
    return render_template('recipes.html')

@app.route('/recipe/<int:recipe_id>')
def recipe(recipe_id):
    return render_template(str(recipe_id) + '.html')

@app.route('/customer_area')
def customer_area():
    return "Customer Area"

@app.route('/shop')
def shop():
    products = [
        {'id': 1, 'name': 'Croissant', 'description': 'Flaky and buttery', 'price': '$2.50', 'image': 'croissant.jpg'},
        {'id': 2, 'name': 'Eclair', 'description': 'Cream-filled delight', 'price': '$3.00', 'image': 'eclair.jpg'},
        {'id': 3, 'name': 'Macaron', 'description': 'French almond meringue cookie', 'price': '$1.50', 'image': 'macaron.jpg'},
        {'id': 4, 'name': 'Tart', 'description': 'Fruity and delicious', 'price': '$2.75', 'image': 'tart.jpg'}
    ]
    return render_template('shop.html', products=products)

@app.route('/shop/<int:product_id>')
def product_page(product_id):
    products = [
        {'id': 1, 'name': 'Croissant', 'description': 'Flaky and buttery', 'price': '$2.50', 'image': 'croissant.jpg'},
        {'id': 2, 'name': 'Eclair', 'description': 'Cream-filled delight', 'price': '$3.00', 'image': 'eclair.jpg'},
        {'id': 3, 'name': 'Macaron', 'description': 'French almond meringue cookie', 'price': '$1.50', 'image': 'macaron.jpg'},
        {'id': 4, 'name': 'Tart', 'description': 'Fruity and delicious', 'price': '$2.75', 'image': 'tart.jpg'}
    ]

    product = next((p for p in products if p['id'] == product_id), None)
    if product:
        return render_template('product_page.html', product=product)
    else:
        return "Product not found", 404

if __name__ == '__main__':
    app.run(debug=True)
